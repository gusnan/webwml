#use wml::debian::translation-check translation="326ce30b40d6fb0de9a5955b4d8b0d7b4d143303"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han encontrado varias vulnerabilidades en Ansible, un sistema para
configuración, gestión, despliegue y ejecución de tareas:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10855">CVE-2018-10855</a>
/ <a href="https://security-tracker.debian.org/tracker/CVE-2018-16876">CVE-2018-16876</a>

    <p>El indicador de tarea no_log no se respetaba, dando lugar a que se filtrara información.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10875">CVE-2018-10875</a>

    <p>Se leía ansible.cfg del directorio de trabajo actual.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16837">CVE-2018-16837</a>

    <p>El módulo user filtraba al entorno del proceso parámetros pasados a
    ssh-keygen.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-3828">CVE-2019-3828</a>

    <p>El módulo fetch era susceptible de escalado de directorios.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 2.2.1.0-2+deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de ansible.</p>

<p>Para información detallada sobre el estado de seguridad de ansible, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/ansible">https://security-tracker.debian.org/tracker/ansible</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4396.data"
