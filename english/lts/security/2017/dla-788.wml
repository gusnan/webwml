<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Florian Heinz and Martin Kluge reported that pdns-recursor, a recursive
DNS server, parses all records present in a query regardless of whether
they are needed or even legitimate, allowing a remote, unauthenticated
attacker to cause an abnormal CPU usage load on the pdns server,
resulting in a partial denial of service if the system becomes
overloaded.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.3-3+deb7u2.</p>

<p>We recommend that you upgrade your pdns-recursor packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-788.data"
# $Id: $
