<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two use-after-free vulnerabilities were discovered in DBD::mysql, a Perl
DBI driver for the MySQL database server. A remote attacker can take
advantage of these flaws to cause a denial-of-service against an
application using DBD::mysql (application crash), or potentially to
execute arbitrary code with the privileges of the user running the
application.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.021-1+deb7u1.</p>

<p>We recommend that you upgrade your libdbd-mysql-perl packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-576.data"
# $Id: $
