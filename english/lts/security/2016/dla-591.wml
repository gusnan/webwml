<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An OpenDocument Presentation .ODP or Presentation Template .OTP file
can contain invalid presentation elements that lead to memory
corruption when the document is loaded in LibreOffice Impress. The
defect may cause the document to appear as corrupted and LibreOffice
may crash in a recovery-stuck mode requiring manual intervention. A
crafted exploitation of the defect can allow an attacker to cause
denial of service (memory corruption and application crash) and
possible execution of arbitrary code.</p>

<p>For Debian 7 <q>Wheezy</q>, this problem have been fixed in version
3.5.4+dfsg2-0+deb7u8.</p>

<p>We recommend that you upgrade your libreoffice packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-591.data"
# $Id: $
