# translation of templates.pl.po to polski
# Wojciech Zaręba <wojtekz@comp.waw.pl>, 2007.
# Wojciech Zareba <wojtekz@comp.waw.pl>, 2007, 2008.
# translation of templates.po to
msgid ""
msgstr ""
"Project-Id-Version: templates.pl\n"
"PO-Revision-Date: 2008-06-16 12:20+0200\n"
"Last-Translator: Wojciech Zareba <wojtekz@comp.waw.pl>\n"
"Language-Team: polski <debian-l10n-polish@lists.debian.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"

#: ../../english/template/debian/votebar.wml:13
msgid "Date"
msgstr "Data"

#: ../../english/template/debian/votebar.wml:16
msgid "Time Line"
msgstr "Termin"

#: ../../english/template/debian/votebar.wml:19
msgid "Summary"
msgstr ""

#: ../../english/template/debian/votebar.wml:22
msgid "Nominations"
msgstr "Nominacje"

#: ../../english/template/debian/votebar.wml:25
msgid "Debate"
msgstr "Debata"

#: ../../english/template/debian/votebar.wml:28
msgid "Platforms"
msgstr "Platformy"

#: ../../english/template/debian/votebar.wml:31
msgid "Proposer"
msgstr "Wnioskodawca"

#: ../../english/template/debian/votebar.wml:34
msgid "Proposal A Proposer"
msgstr "Propozycja wnioskodawcy A"

#: ../../english/template/debian/votebar.wml:37
msgid "Proposal B Proposer"
msgstr "Propozycja wnioskodawcy B"

#: ../../english/template/debian/votebar.wml:40
msgid "Proposal C Proposer"
msgstr "Propozycja wnioskodawcy C"

#: ../../english/template/debian/votebar.wml:43
msgid "Proposal D Proposer"
msgstr "Propozycja wnioskodawcy D"

#: ../../english/template/debian/votebar.wml:46
msgid "Proposal E Proposer"
msgstr "Propozycja wnioskodawcy E"

#: ../../english/template/debian/votebar.wml:49
msgid "Proposal F Proposer"
msgstr "Propozycja wnioskodawcy F"

#: ../../english/template/debian/votebar.wml:52
msgid "Seconds"
msgstr "Popierający"

#: ../../english/template/debian/votebar.wml:55
msgid "Proposal A Seconds"
msgstr "Popierający propozycję A"

#: ../../english/template/debian/votebar.wml:58
msgid "Proposal B Seconds"
msgstr "Popierający propozycję B"

#: ../../english/template/debian/votebar.wml:61
msgid "Proposal C Seconds"
msgstr "Popierający propozycję C"

#: ../../english/template/debian/votebar.wml:64
msgid "Proposal D Seconds"
msgstr "Popierający propozycję D"

#: ../../english/template/debian/votebar.wml:67
msgid "Proposal E Seconds"
msgstr "Popierający propozycję E"

#: ../../english/template/debian/votebar.wml:70
msgid "Proposal F Seconds"
msgstr "Popierający propozycję F"

#: ../../english/template/debian/votebar.wml:73
msgid "Opposition"
msgstr "Przeciwni"

#: ../../english/template/debian/votebar.wml:76
msgid "Text"
msgstr "Tekst"

#: ../../english/template/debian/votebar.wml:79
msgid "Proposal A"
msgstr "Propozycja A"

#: ../../english/template/debian/votebar.wml:82
msgid "Proposal B"
msgstr "Propozycja B"

#: ../../english/template/debian/votebar.wml:85
msgid "Proposal C"
msgstr "Propozycja C"

#: ../../english/template/debian/votebar.wml:88
msgid "Proposal D"
msgstr "Propozycja D"

#: ../../english/template/debian/votebar.wml:91
msgid "Proposal E"
msgstr "Propozycja E"

#: ../../english/template/debian/votebar.wml:94
msgid "Proposal F"
msgstr "Propozycja F"

#: ../../english/template/debian/votebar.wml:97
msgid "Choices"
msgstr "Wybory"

#: ../../english/template/debian/votebar.wml:100
msgid "Amendment Proposer"
msgstr "Proponujący poprawkę"

#: ../../english/template/debian/votebar.wml:103
msgid "Amendment Seconds"
msgstr "Popierający poprawkę"

#: ../../english/template/debian/votebar.wml:106
msgid "Amendment Text"
msgstr "Tekst poprawki"

#: ../../english/template/debian/votebar.wml:109
msgid "Amendment Proposer A"
msgstr "Proponujący poprawkę A"

#: ../../english/template/debian/votebar.wml:112
msgid "Amendment Seconds A"
msgstr "Popierający poprawkę A"

#: ../../english/template/debian/votebar.wml:115
msgid "Amendment Text A"
msgstr "Tekst poprawki A"

#: ../../english/template/debian/votebar.wml:118
msgid "Amendment Proposer B"
msgstr "Proponujący poprawkę B"

#: ../../english/template/debian/votebar.wml:121
msgid "Amendment Seconds B"
msgstr "Popierający poprawkę B"

#: ../../english/template/debian/votebar.wml:124
msgid "Amendment Text B"
msgstr "Tekst poprawki B"

#: ../../english/template/debian/votebar.wml:127
#, fuzzy
#| msgid "Amendment Proposer A"
msgid "Amendment Proposer C"
msgstr "Proponujący poprawkę A"

#: ../../english/template/debian/votebar.wml:130
#, fuzzy
#| msgid "Amendment Seconds A"
msgid "Amendment Seconds C"
msgstr "Popierający poprawkę A"

#: ../../english/template/debian/votebar.wml:133
#, fuzzy
#| msgid "Amendment Text A"
msgid "Amendment Text C"
msgstr "Tekst poprawki A"

#: ../../english/template/debian/votebar.wml:136
msgid "Amendments"
msgstr "Poprawki"

#: ../../english/template/debian/votebar.wml:139
msgid "Proceedings"
msgstr "Postępowanie"

#: ../../english/template/debian/votebar.wml:142
msgid "Majority Requirement"
msgstr "Konieczna większość"

#: ../../english/template/debian/votebar.wml:145
msgid "Data and Statistics"
msgstr "Dane i statystyki"

#: ../../english/template/debian/votebar.wml:148
msgid "Quorum"
msgstr "Kworum"

#: ../../english/template/debian/votebar.wml:151
msgid "Minimum Discussion"
msgstr "Dyskusja"

#: ../../english/template/debian/votebar.wml:154
msgid "Ballot"
msgstr "Głosowanie"

#: ../../english/template/debian/votebar.wml:157
msgid "Forum"
msgstr "Forum"

#: ../../english/template/debian/votebar.wml:160
msgid "Outcome"
msgstr "Wynik"

#: ../../english/template/debian/votebar.wml:164
msgid "Waiting&nbsp;for&nbsp;Sponsors"
msgstr "Czeka&nbsp;na&nbsp;Sponsora"

#: ../../english/template/debian/votebar.wml:167
msgid "In&nbsp;Discussion"
msgstr "Dyskusja&nbsp;trwa"

#: ../../english/template/debian/votebar.wml:170
msgid "Voting&nbsp;Open"
msgstr "Głosowanie&nbsp;Otwarte"

#: ../../english/template/debian/votebar.wml:173
msgid "Decided"
msgstr "Zadecydowano"

#: ../../english/template/debian/votebar.wml:176
msgid "Withdrawn"
msgstr "Wycofane"

#: ../../english/template/debian/votebar.wml:179
msgid "Other"
msgstr "Inne"

#: ../../english/template/debian/votebar.wml:183
msgid "Home&nbsp;Vote&nbsp;Page"
msgstr "Główna&nbsp;Strona&nbsp;Głosowań"

#: ../../english/template/debian/votebar.wml:186
msgid "How&nbsp;To"
msgstr "Jak"

#: ../../english/template/debian/votebar.wml:189
msgid "Submit&nbsp;a&nbsp;Proposal"
msgstr "Zgłosić&nbsp;Propozycję"

#: ../../english/template/debian/votebar.wml:192
msgid "Amend&nbsp;a&nbsp;Proposal"
msgstr "Poprawić&nbsp;Propozycję"

#: ../../english/template/debian/votebar.wml:195
msgid "Follow&nbsp;a&nbsp;Proposal"
msgstr "Poprzeć&nbsp;Propozycję"

#: ../../english/template/debian/votebar.wml:198
msgid "Read&nbsp;a&nbsp;Result"
msgstr "Przeczytać&nbsp;Rezultat"

#: ../../english/template/debian/votebar.wml:201
msgid "Vote"
msgstr "Głosować"
