#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité a été découverte dans exiv2
(<a href="https://security-tracker.debian.org/tracker/CVE-2018-16336">CVE-2018-16336</a>),
une bibliothèque C++ et un utilitaire en ligne de commande pour gérer les
métadonnées d’image, aboutissant à un déni de service distant (une lecture
hors limites de tampon basé sur le tas) à l'aide d'un fichier d’image contrefait.</p>

<p>De plus, cette mise à jour inclut une modification mineure au correctif pour
<a href="https://security-tracker.debian.org/tracker/CVE-2018-10958">CVE-2018-10958</a>/<a href="https://security-tracker.debian.org/tracker/CVE-2018-10999">CVE-2018-10999</a>, une vulnérabilité corrigée
une première fois dans DLA-1402-1. Le correctif initial était exagérément
restrictif et a été ajusté pour supprimer cette restriction excessive.</p>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 0.24-4.1+deb8u2.</p>.
<p>Nous vous recommandons de mettre à jour vos paquets exiv2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1551.data"
# $Id: $
