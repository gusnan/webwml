#use wml::debian::mainpage title="Le système d'exploitation universel" GEN_TIME="yes"
#use wml::debian::recent_list
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/images.data"
#use wml::debian::translation-check translation="db28b03ffeea30dd379cb4120e6f83d0e85722d8" maintainer="Jean-Paul Guillonneau"

#traducteurs et contributeurs, voir le journal :
#https://anonscm.debian.org/viewvc/webwml/webwml/french/index.wml?diff_format=h&view=log

<span class="download">
<a href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">
Télécharger Debian <current_release_short><em> (installation par le réseau pour PC 64 bits)</em></a> </span>





<div id="splash" style="text-align: center;">
       <h1>Debian</h1>
</div>

<div id="intro">
<p>Debian est un système d'exploitation
<a href="intro/free">libre</a> pour votre ordinateur. Un système d'exploitation
est une suite de programmes de base et d’utilitaires permettant à un ordinateur
de fonctionner.
</p>

<p>Debian est bien plus qu'un simple système d'exploitation&nbsp;: il est
fourni avec plus de <packages_in_stable> <a href="distrib/packages">paquets</a>&nbsp;.
Ce sont des composants logiciels précompilés, assemblés dans un format
ingénieux conçu pour une installation aisée sur une machine.
<a href="intro/about">Lire la suite…</a>
</div>
<hometoc/>

<p class="infobar">
La <a href="releases/stable/">dernière version stable de Debian</a> est
la <current_release_short>. La dernière mise à jour de cette version
a été publiée le <current_release_date>.

Vous pouvez aussi accéder aux
<a href="releases/">autres versions disponibles de Debian</a>.</p>



<h2>Pour commencer</h2>
<p>Veuillez utiliser la barre de navigation au sommet de cette page pour accéder
à plus de contenus.</p>

<p>De plus, les utilisateurs qui parlent une langue autre que l'anglais peuvent
consulter la section sur l'<a href="international/">international</a>, et ceux
utilisant un autre système qu’Intel x86 peuvent consulter la section sur les
<a href="ports/">portages</a>.

<hr />
<a class="rss_logo" href="News/news">RSS</a>
<h2>Actualités</h2>

<p><:= get_recent_list('News/$(CUR_YEAR)', '6', '$(ENGLISHDIR)', '', '\d+\w*' ) :></p>

<p>Pour les communiqués plus anciens, consultez les pages <a href="$(HOME)/News/">actualités</a>.
Si vous voulez recevoir un courrier (en anglais) à chaque fois qu'un communiqué paraît, abonnez-vous
à la <a href="MailingLists/debian-announce">liste de diffusion debian-announce</a>.</p>

<hr />
<a class="rss_logo" href="security/dsa">RSS</a>
<h2>Annonces de sécurité</h2>

<p><:= get_recent_list ('security/2w', '10', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)' ) :></p>

<p>Pour les annonces de sécurité, consultez les <a href="$(HOME)/security/">\
informations de sécurité</a>.
Si vous voulez recevoir les annonces de sécurité (en anglais) dès leur parution, abonnez-vous
à la <a href="https://lists.debian.org/debian-security-announce/">liste de diffusion debian-security-announce</a>.</p>

{#rss#:
<link rel="alternate" type="application/rss+xml"
 title="Actualités Debian" href="News/news">
<link rel="alternate" type="application/rss+xml"
 title="Nouvelles du projet Debian" href="News/weekly/dwn">
<link rel="alternate" type="application/rss+xml"
 title="Annonces de sécurité Debian (titres seuls)" href="security/dsa">
<link rel="alternate" type="application/rss+xml"
 title="Annonces de sécurité Debian (résumés)" href="security/dsa-long">
:#rss#}
