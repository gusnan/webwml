#use wml::debian::template title="Requerimentos do estande"
#use wml::debian::translation-check translation="fbb69c19d5d26ddbf92456e6913376d5fcaa9c16" translation_maintainer="Paulo Henrique de Lima Santana (phls)"

<p>Ocorreram inúmeros problemas com a organização de estandes no
passado. Este documento visa lançar alguma luz sobre o assunto.
</p>

<p>Antes de mais nada, o Projeto Debian não possui dinheiro para
gastar em exposições, especialmente em materiais não retornáveis (como espaço
físico, carpetes, mesas, etc). Sendo assim, o projeto precisa ter todo o estande
patrocinado pela organização do evento, e não apenas o espaço físico, a fim
de manter um estande adequado e bem equipado.
</p>

<p>Em segundo lugar, tudo que é mostrado em um estande Debian durante uma
exposição é doado por pessoas ou empresas que nos oferecem formas de nos
apoiarem. Todo equipamento é mantido por pessoas do Debian que estão
interessadas em organizar o estande. Todas as despesas, seja de hospedagem ou
de viagem, são normalmente pagas do próprio bolso por estas pessoas.
Todas as outras despesas que fazem o estande parecer atrativo (por exemplo
banners, cartazes, camisetas, etc) são também pagas por essas pessoas.
</p>

<p>Isto siginifica que fornecer somente o espaço físico para um estande, faria
com que o mesmo parecesse bastante pobre, uma vez que a falta de
eletricidade significaria que computadores não poderiam exibir coisa
alguma (uma vez que eletricidade é necessária para ligar as máquinas
e monitores), ou somente laptops poderiam ser usados, mas eles podem
armazenar energia por não mais do que quatro horas. Além disso, também
siginificaria que as pessoas que estão trabalhando no estande, teriam que sentar
no chão caso ninguém fornecesse mesas, cadeiras e um carpete.
</p>

<p>Resumindo, um estande como esse pareceria muito pobre. É bastante
questionável se isso demonstraria o alto nível de uma exposição e um projeto
pobre, ou mostraria que a organização do evento não têm dinheiro ou não querem gastar
dinheiro em projetos de software livre.
</p>

<p>Afinal, tais projetos de software livre criam os produtos pelos quais os(as)
organizadores(as) conseguem ser pagos. Sem todos estes projetos fornecidos por este
maravilhoso software livre, não existiriam conferências cobrindo este mesmo software.
Não existiriam também muitas empresas que baseiam seus negócios neste tipo de
software. Levando isto em consideração, onde está o problema para os(as)
organizadores(as) das conferências fornecerem um estande completo para esses projetos?
Quanto é $500 gastos em um estande, para uma empresa que cobra de uma única
pessoa uma taxa de entrada que é mais do que isso?
</p>
