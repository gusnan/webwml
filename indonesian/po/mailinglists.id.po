msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2016-08-28 00:01+0700\n"
"Last-Translator: Izharul Haq <atoz.chevara@yahoo.com>\n"
"Language-Team: Debian Indonesia Translator <debian-l10n@debian-id.org>\n"
"Language: id_ID\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#: ../../english/MailingLists/mklist.tags:6
msgid "Mailing List Subscription"
msgstr "Pendaftaran Mailing List"

#: ../../english/MailingLists/mklist.tags:9
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to subscribe using e-mail. An <a href=\"unsubscribe\">unsubscription web "
"form</a> is also available, for unsubscribing from mailing lists. "
msgstr ""
"Lihat halaman <a href=\"./#subunsub\">mailing lists</a> untuk informasi "
"bagaimana mendaftarkan dengan email. <a href=\"unsubscribe\">Pembatalan "
"pendaftaran lewat web</a> juga tersedia untuk pembatalan milis. "

#: ../../english/MailingLists/mklist.tags:12
msgid ""
"Note that most Debian mailing lists are public forums. Any mails sent to "
"them will be published in public mailing list archives and indexed by search "
"engines. You should only subscribe to Debian mailing lists using an e-mail "
"address that you do not mind being made public."
msgstr ""
"Perhatikan bahwa sebagian besar milis Debian adalah Forum publik. Semua "
"emaul yang dikirimkan akan dipublikasikan di arsip publik milis dan di "
"indexkan oleh mesin pencari. Anda dapat hanya mendaftarkan ke Milis debian "
"dengan menggunakan alamat email yang tidak masalah jika digunakan di publik."

#: ../../english/MailingLists/mklist.tags:15
msgid ""
"Please select which lists you want to subscribe to (the number of "
"subscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Silahkan pilih milis yang ingin anda ikuti (jumlah pelanggan terbatas, jika "
"permintaan anda tidak berhasil, silahkan gunakan <a href=\"./#subunsub"
"\">metode lainnya</a>):"

#: ../../english/MailingLists/mklist.tags:18
msgid "No description given"
msgstr "Tidak ada deskripsi yang diberikan"

#: ../../english/MailingLists/mklist.tags:21
msgid "Moderated:"
msgstr "Dimoderasikan:"

#: ../../english/MailingLists/mklist.tags:24
msgid "Posting messages allowed only to subscribers."
msgstr "Mengirimkan berita hanya diijinkan oleh pelanggan."

#: ../../english/MailingLists/mklist.tags:27
msgid ""
"Only messages signed by a Debian developer will be accepted by this list."
msgstr ""
"Hanya pesan yang ditandatangani oleh pengembang Debian yang disetujui oleh "
"milis ini."

#: ../../english/MailingLists/mklist.tags:30
msgid "Subscription:"
msgstr "Pendaftaran:"

#: ../../english/MailingLists/mklist.tags:33
msgid "is a read-only, digestified version."
msgstr "Hanya baca, versi digest"

#: ../../english/MailingLists/mklist.tags:36
msgid "Your E-Mail address:"
msgstr "Elamat email anda:"

#: ../../english/MailingLists/mklist.tags:39
msgid "Subscribe"
msgstr "Berlangganan"

#: ../../english/MailingLists/mklist.tags:42
msgid "Clear"
msgstr "Bersihkan"

#: ../../english/MailingLists/mklist.tags:45
msgid ""
"Please respect the <a href=\"./#ads\">Debian mailing list advertising "
"policy</a>."
msgstr ""
"Mohon menghormati <a href=\"./#ads\">Aturan periklanan Milis Debian</a>."

#: ../../english/MailingLists/mklist.tags:48
msgid "Mailing List Unsubscription"
msgstr "Pembatalan pendaftaran Milis"

#: ../../english/MailingLists/mklist.tags:51
msgid ""
"See the <a href=\"./#subunsub\">mailing lists</a> page for information on "
"how to unsubscribe using e-mail. An <a href=\"subscribe\">subscription web "
"form</a> is also available, for subscribing to mailing lists. "
msgstr ""
"Lihat halaman <a href=\"./#subunsub\">milis</a> untuk informasi bagaimana "
"membatalkan langganan menggunakan email. <a href=\"subscribe\">Form "
"pendaftaran web</a> juga tersedia untuk mendaftarkan ke milis. "

#: ../../english/MailingLists/mklist.tags:54
msgid ""
"Please select which lists you want to unsubscribe from (the number of "
"unsubscriptions is limited, if your request doesn't succeed, please use <a "
"href=\"./#subunsub\">another method</a>):"
msgstr ""
"Silahkan pilih milis yang ingin anda tinggalkan (jumlah pembatalan "
"berlangganan terbatas, jika permintaan anda tidak berhasil, silahkan gunakan "
"<a href=\"./#subunsub\">metode lainnya</a>):"

#: ../../english/MailingLists/mklist.tags:57
msgid "Unsubscribe"
msgstr "Batalkan berlangganan"

#: ../../english/MailingLists/mklist.tags:60
msgid "open"
msgstr "buka"

#: ../../english/MailingLists/mklist.tags:63
msgid "closed"
msgstr "tertutup"

#~ msgid "Please select which lists you want to subscribe to:"
#~ msgstr "Silakan pilih milis mana yang anda ingin mendaftar:"

#~ msgid "Please select which lists you want to unsubscribe from:"
#~ msgstr "Silakan pilih milis mana yang akan anda batalkan langganannya:"
